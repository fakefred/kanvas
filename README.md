# Kanvas

Kanvas is a plasmoid that syncs announcements and assignments from your
university's Canvas®™© LMS to your Plasma desktop. It lacks features and
configs, and I would not say it is production-ready.

Kanvas is written in QML. It is the first QML application I wrote and
I have not touched JavaScript in two years, so the code sucks.

Right now, it looks like this:

![Screenshot](img/screenshot_0.3.0.png)

## Development

First obviously you need the Plasma framework. If you have zero Plasma
development experience like me, here are some useful links:

- [Techbase: Plasma 5](https://techbase.kde.org/Development/Tutorials/Plasma5)
- [Plasma QML API](https://api.kde.org/frameworks/plasma-framework/html/index.html)
- [Developer: Plasma Widget Testing](https://develop.kde.org/docs/plasma/widget/testing/)

After you clone this repo, run `make install` to copy the plasmoid to
`~/.local/share/plasma/plasmoids/`. You need to run it only once.

Every time you want to test Kanvas, run `make` to update the plasmoid in
`~/.local/` and run it in a window (`plasmawindowed`).

Then, configure the plasmoid. Right click the "Kanvas" heading and click
"Configure Kanvas". Go to the "Canvas" tab and fill out your Canvas domain
and OAuth2 token. Click "Fetch courses", then follow instructions on the
UI. Click "OK" to save. The configuration will be saved for plasmawindowed
every time you debug, but kept separate from the rest of Plasma; if you
add the widget to your desktop it has to be configured again.

## Todo

- [ ] Sort assignments by urgency
- [ ] More friendly course list in config dialog
- [ ] Ignore activity and undo ignore
- [ ] Migrate to KF6 when it replaces KF5 (see [Porting guide](https://develop.kde.org/docs/plasma/widget/porting_kf6/))
- [ ] Improve accessibility [with help of this article](https://carlschwan.eu/2023/07/30/debugging-the-keyboard-navigation-in-your-qml-application/)

## Contributing

I am terrible at QML. Help me.

- [KDE GitLab](https://invent.kde.org/fakefred/kanvas)
- [SourceHut](https://git.sr.ht/~fkfd/kanvas), mailing list for git
  send-email: `~fkfd/misc@lists.sr.ht`

## Related "documentation"

There's hardly any documentation on integrating QML with the rest of
Plasma. For example, when I wanted a function that tells me whether the
machine is online, all I found was the source code along with a header
file. I will list things like these so you don't have to.

- [plasma-nm, networkmanagement](https://invent.kde.org/plasma/plasma-nm/-/blob/master/libs/declarative/networkstatus.h)
