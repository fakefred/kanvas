import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import org.kde.kirigami as Kirigami

Kirigami.FormLayout {
    property alias cfg_showSubmittedAssignments: showSubmittedAssignments.checked
    property alias cfg_refreshInterval: refreshInterval.value

    SpinBox {
        id: refreshInterval
        Kirigami.FormData.label: i18n("Refresh interval (minutes):")
        editable: true
        from: 1
        to: 1440
    }

    CheckBox {
        id: showSubmittedAssignments
        text: i18n("Show submitted assignments")
    }
}

