import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import org.kde.plasma.plasmoid
import org.kde.plasma.core as PlasmaCore
import org.kde.plasma.components as PlasmaComponents3
import org.kde.plasma.extras as PlasmaExtras
import org.kde.kirigami as Kirigami

import "activity.js" as Activity

// container for one single activity
PlasmaExtras.ListItem {
    id: activityItem
    separatorVisible: false
    enabled: true // enable mouse event handling
    property bool expanded: false

    ColumnLayout {
        width:
            (type == "announcement"
            ? announcementsListView : assignmentsListView).width
            - Kirigami.Units.gridUnit // HACK: clearance for scrollbar
        Layout.fillWidth: true
        RowLayout {
            // "finished" checkbox and activity title
            RowLayout {
                PlasmaComponents3.CheckBox {
                    id: activityCheckbox
                    checkState: finished ? Qt.Checked : Qt.Unchecked
                    onToggled: () => {
                        finished = (checkState == Qt.Checked)
                        activityLabel.font.strikeout = (checkState == Qt.Checked)

                        const configKeys = {
                            announcement: "finishedAnnouncements",
                            assignment: "finishedAssignments",
                        }

                        let finishedActivities = plasmoid.configuration[configKeys[type]]
                        const activityIdStr = activityId.toString()
                        if (finished) {
                            if (!finishedActivities.includes(activityIdStr)) {
                                finishedActivities.push(activityIdStr)
                            }
                        } else {
                            // remove activityIdStr from list
                            finishedActivities.splice(
                                finishedActivities.indexOf(activityIdStr), 1
                            )
                        }
                        // save config
                        plasmoid.configuration[configKeys[type]] = finishedActivities
                    }
                }

                ColumnLayout {
                    PlasmaComponents3.Label {
                        id: activityLabel
                        text: `[${course}] ${title}`
                        font.bold: important
                        font.strikeout: finished
                        color: important
                            ? PlasmaCore.Theme.negativeTextColor
                            : PlasmaCore.Theme.textColor
                        wrapMode: Text.WordWrap
                        Layout.fillWidth: true
                    }

                    PlasmaComponents3.Label {
                        id: dueLabel
                        visible: type == "assignment"
                        text: Activity.dueLabelText()
                        color: activityLabel.color
                        Layout.fillWidth: true
                    }

                    MouseArea {
                        width: parent.width
                        height: parent.height
                        cursorShape: Qt.PointingHandCursor
                        onClicked: () => {
                            if (type == "announcement") {
                                expanded = !expanded
                            } else {
                                Qt.openUrlExternally(url)
                            }
                        }
                    }
                }
            }

            // buttons
            RowLayout {
                Layout.alignment: Qt.AlignRight
                PlasmaComponents3.ToolButton {
                    icon.name: "emblem-important-symbolic"
                    checked: important
                    opacity: activityItem.containsMouse ? 1 : 0

                    onClicked: () => {
                        important = !important
                        activityLabel.font.bold = important
                        activityLabel.color = important
                            ? Kirigami.Theme.negativeTextColor
                            : Kirigami.Theme.textColor

                        const configKeys = {
                            announcement: "importantAnnouncements",
                            assignment: "importantAssignments",
                        }

                        let importantActivities = plasmoid.configuration[configKeys[type]]
                        const activityIdStr = activityId.toString()

                        if (important) {
                            if (!importantActivities.includes(activityIdStr)) {
                                importantActivities.push(activityIdStr)
                            }
                        } else {
                            // remove activityIdStr from list
                            importantActivities.splice(
                                importantActivities.indexOf(activityIdStr), 1
                            )
                        }
                        // save config
                        plasmoid.configuration[configKeys[type]] = importantActivities
                    }
                }
            }
        }

        // announcement message
        PlasmaComponents3.Label {
            Layout.fillWidth: true
            visible: expanded
            text: message
            horizontalAlignment: Text.AlignLeft
        }

        PlasmaComponents3.Button {
            icon.name: "internet-web-browser"
            visible: expanded
            text: i18n("Open in browser")
            onClicked: () => {
                Qt.openUrlExternally(url)
            }
        }
    }
}
