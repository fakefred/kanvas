import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import org.kde.plasma.plasmoid
import org.kde.plasma.core as PlasmaCore
import org.kde.plasma.components as PlasmaComponents3
import org.kde.plasma.extras as PlasmaExtras
import org.kde.plasma.networkmanagement as PlasmaNM
import org.kde.kirigami as Kirigami

import "kanvas.js" as Kanvas

PlasmoidItem {
    width: Kirigami.Units.gridUnit * 20
    height: Kirigami.Units.gridUnit * 40
    clip: true

    readonly property string canvasUrl: plasmoid.configuration.canvasUrl
    readonly property string apiEndpoint: `${canvasUrl.replace(/\/$/, "")}/api/v1`
    readonly property string oauth2Token: plasmoid.configuration.oauth2Token
    readonly property string authHeader: `Bearer ${oauth2Token}`

    PlasmaNM.NetworkStatus {
        id: networkStatus
    }

    // sync on initialization
    Component.onCompleted: Kanvas.syncCanvas()

    // update every refreshInterval minutes
    Timer {
        interval: plasmoid.configuration.refreshInterval * 60 * 1000
        running: true
        repeat: true
        onTriggered: Kanvas.syncCanvas()
    }

    // top level layout
    ColumnLayout {
        id: main
        anchors.fill: parent

        PlasmaExtras.Heading {
            level: 1
            text: i18n("Kanvas")
        }

        PlasmaExtras.Heading {
            level: 2
            text: i18n("Announcements")
        }

        ListModel {
            id: announcementsModel
            /* Uncomment when debugging layout
            ListElement {
                type: "announcement"
                course: "CS101"
                title: "Code quality"
                message: "Code may be a little messy…"
                url: "https://xkcd.com/1513"
                important: true
                finished: false
                activityId: 0
            }
            */
        }

        ScrollView {
            implicitHeight: Kirigami.Units.gridUnit * 20
            Layout.margins: Kirigami.Units.smallSpacing
            Layout.fillWidth: true
            Layout.fillHeight: true
            ScrollBar.horizontal.policy: ScrollBar.AlwaysOff

            ListView {
                id: announcementsListView
                Layout.fillWidth: true
                spacing: 0
                delegate: ActivityView {}
                model: announcementsModel

                PlasmaExtras.PlaceholderMessage {
                    anchors.centerIn: parent
                    width: parent.width - (Kirigami.Units.gridUnit * 4)
                    visible: announcementsModel.count == 0
                    iconName: "mail-read-symbolic"
                    text: i18n("No announcements")
                }
            }
        }

        PlasmaExtras.Heading {
            level: 2
            text: i18n("Assignments")
        }

        ListModel {
            id: assignmentsModel
            /* Uncomment when debugging layout
            ListElement {
                type: "assignment"
                course: "EE201"
                title: "Circuit diagram"
                dueAt: "2022-04-10T15:59:59Z"
                submitted: true
                url: "https://xkcd.com/730"
                important: true
                finished: true
                activityId: 1
            }
            */
        }

        ScrollView {
            implicitHeight: Kirigami.Units.gridUnit * 20
            Layout.margins: Kirigami.Units.smallSpacing
            Layout.fillWidth: true
            Layout.fillHeight: true
            ScrollBar.horizontal.policy: ScrollBar.AlwaysOff

            ListView {
                id: assignmentsListView
                Layout.fillWidth: true
                spacing: Kirigami.Units.smallSpacing
                delegate: ActivityView {}
                model: assignmentsModel

                PlasmaExtras.PlaceholderMessage {
                    anchors.centerIn: parent
                    width: parent.width - (Kirigami.Units.gridUnit * 4)
                    visible: assignmentsModel.count == 0
                    iconName: "mail-read-symbolic"
                    text: i18n("No assignments")
                }
            }
        }

        RowLayout {
            PlasmaComponents3.Button {
                icon.name: "view-refresh"
                text: i18n("Refresh")
                onClicked: Kanvas.syncCanvas()
            }

            // PlasmaComponents3.Label {
                // id: offlineLabel
                // visible: networkStatus.networkStatus != "Connected"
                // text: i18n("Network disconnected")
                // color: PlasmaCore.Theme.negativeTextColor
            // }
        }
    }
}
