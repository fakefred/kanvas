# for development only

all: update run

install:
	kpackagetool5 -i ./

update:
	kpackagetool5 -u ./

run:
	plasmawindowed me.fkfd.kanvas
